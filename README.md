Setup:  
0. Download cert files (`keystore.jks`) to `certs/`, update settings in `config.yml` (copy from `config-example.yml`)    
1. `python3.9 -m venv env`  
2. `source env/bin/activate`  
3. `python3 -m pip install -r requirements.txt`
4. `python3 main.py`
