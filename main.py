import csv
import json
import asyncio
import aiohttp
from pprint import pprint

from auth import get_token, load_config


def generate_dummy(fname="dummy.csv", size=50):
    """used as an example file for uploading to sf"""
    with open(fname, "w") as f:
        writer = csv.writer(f)
        rows, cols = (size, size)
        writer.writerow(list(f"column {i+1}" for i in range(cols)))
        for j in range(rows):
            writer.writerow(list(i * j for i in range(cols)))


async def main(config):
    token = await get_token(config)
    headers = {"Authorization": f"Bearer {token}"}
    base_url = config["base_url"]

    async with aiohttp.ClientSession(headers=headers) as session:

        async def f(path, query=None):
            async with session.get(base_url + path, params=query) as response:
                assert response.status == 200, f"fuck ({response.status})"
                payload = await response.json()
                return payload

        # root api versions (v50.0 is latest)
        # o = await f("/services/data/")

        # list available api endpoints
        # o = await f("/services/data/v50.0")

        # list account names
        query = "SELECT name from Account"
        query = "Select Id, Title, FileExtension, LatestPublishedVersionId, CreatedDate, ContentSize From ContentDocument"

        response = await f("/services/data/v50.0/query/", {"q": query})
        for record in response["records"]:
            version_id, file_title, file_ext, file_size = (
                record[k]
                for k in [
                    "LatestPublishedVersionId",
                    "Title",
                    "FileExtension",
                    "ContentSize",
                ]
            )

            file_name = f"{file_title}.{file_ext}"
            print(file_name, file_size)

            url = f"{base_url}/services/data/v50.0/sobjects/ContentVersion/{version_id}/VersionData"
            async with session.get(url) as response:
                assert response.status == 200, f"fuck ({response.status})"
                body = await response.read()
                print(len(body))


if __name__ == "__main__":
    config = load_config()
    try:
        asyncio.run(main(config))
    except KeyboardInterrupt:
        pass
