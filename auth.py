import jwt
import jks
import json
import yaml
import base64
import asyncio
import aiohttp
from pathlib import Path
from OpenSSL.crypto import FILETYPE_ASN1, load_privatekey, sign
from datetime import datetime, timedelta
from urllib.parse import urlencode, urlunparse, urlparse

"""
Steps for setting this up from:  
https://help.salesforce.com/articleView?id=sf.remoteaccess_oauth_jwt_flow.htm&type=5
"""


async def authorize(client_id: str):
    """necessary to run this from the browser to confirm application permissions"""
    args = {
        "client_id": client_id,
        "redirect_uri": "https://login.salesforce.com/services/oauth2/success",
        "response_type": "code",
    }
    parts = list(urlparse("https://login.salesforce.com/services/oauth2/authorize"))
    parts[4] = urlencode(args)
    print(f"enter this url in your browser: {urlunparse(parts)}")
    input()


def create_jwt(headers, payload) -> str:
    """not sure how to do this with jwt library so reimlemented here"""
    f = lambda o: base64.urlsafe_b64encode(json.dumps(o).encode()).decode()
    jwt = ".".join(map(f, [headers, payload]))
    return jwt


def sign_jwt(jwt: str, pkey) -> str:
    """load keystore after downloading from sf. locked with account password"""
    signed = sign(pkey, jwt.encode(), "sha256")
    signature = base64.urlsafe_b64encode(signed).decode()
    return f"{jwt}.{signature}"


async def get_token(config):
    """check for cached token "access_token" otherwise fetch new one"""
    access_token_path = Path.cwd() / "access_token"

    if access_token_path.is_file():
        with open(access_token_path, "r") as f:
            access_token = f.read().strip()
        return access_token

    # only necessary once per sf app
    await authorize(config["client_id"])

    headers = {"alg": "RS256"}
    payload = {
        "iss": config["client_id"],
        "aud": "https://login.salesforce.com",
        "sub": config["username"],
        "exp": int((datetime.utcnow() + timedelta(minutes=2)).timestamp()),
    }
    ks = jks.KeyStore.load("certs/keystore.jks", config["keystore_password"])
    pk_entry = next(iter(ks.private_keys.values()))
    pkey = load_privatekey(FILETYPE_ASN1, pk_entry.pkey)
    jwt = sign_jwt(create_jwt(headers, payload), pkey)

    async with aiohttp.ClientSession() as session:
        params = {
            "grant_type": "urn:ietf:params:oauth:grant-type:jwt-bearer",
            "assertion": jwt,
        }
        response = await session.post(
            f"{config['base_url']}/services/oauth2/token", params=params
        )
        if response.status != 200:
            print(await response.text())
            raise Exception("token retrieval failed")

        obj = await response.json()
        # also includes token_type, scope, instance_url, (user) id
        access_token = obj["access_token"]

        with open(access_token_path, "w") as f:
            f.write(access_token)

        return access_token


def load_config(file_path="config.yaml"):
    with open(file_path, "r") as f:
        try:
            config = yaml.safe_load(f)
            # retrieved from "Setup > My Domain"
            config["base_url"] = f"https://{config['instance']}.my.salesforce.com"
            return config
        except yaml.YAMLError as e:
            print("invalid config file")
            raise e


async def test(config):
    token = await get_token(config)
    headers = {"Authorization": f"Bearer {token}"}

    async with aiohttp.ClientSession(headers=headers) as session:
        base_url = config["base_url"]
        url = f"{base_url}/services/data/"
        async with session.get(url) as response:
            assert response.status == 200, f"fuck ({response.status})"
            payload = await response.json()
            print(f"success! latest api version: v{payload[-1]['version']}")


if __name__ == "__main__":
    config = load_config()

    try:
        asyncio.run(test(config))
    except KeyboardInterrupt:
        pass
